package id.kelompok2.miniproject.repository;

import id.kelompok2.miniproject.model.entity.LemariEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LemariRepository extends JpaRepository<LemariEntity, Integer> {
    @Query(value = "select * from tabel_lemari where id_jenis =?1", nativeQuery = true)
    List<LemariEntity> findAllByJenisEntity(Integer jenisEntity);

    @Query (value = "select id_lemari from tabel_lemari where kode_lemari = ?", nativeQuery = true)
    Integer FindIdByKodeLemari (String namaPengarang);
}
