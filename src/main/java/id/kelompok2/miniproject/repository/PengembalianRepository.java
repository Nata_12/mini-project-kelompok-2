package id.kelompok2.miniproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.kelompok2.miniproject.model.entity.GagalKembaliEntity;
import id.kelompok2.miniproject.model.entity.PengembalianEntity;

public interface PengembalianRepository extends JpaRepository<PengembalianEntity, Integer> {
    @Query(value="select * from tabel_pengembalian where kode_peminjaman = ?1", nativeQuery = true)
    PengembalianEntity findkembalibykode(String kodePeminjaman);
}
