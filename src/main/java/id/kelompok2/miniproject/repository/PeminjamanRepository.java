package id.kelompok2.miniproject.repository;

import java.util.List;

import id.kelompok2.miniproject.model.dto.MemberTerbaikDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.kelompok2.miniproject.model.entity.PeminjamanEntity;

public interface PeminjamanRepository extends JpaRepository<PeminjamanEntity, Integer> {
	@Query(value = "select * from public.tabel_peminjaman where username = ?1 ", nativeQuery = true)
	List<PeminjamanEntity> findAllByUsername(String username);
	@Query(value = "select * from tabel_peminjaman where kode_peminjaman = ?1 ", nativeQuery = true)
	PeminjamanEntity findAllByUsernameLike (String kodePeminjaman);
	@Query(value = "select * from tabel_peminjaman where kode_peminjaman = ?1", nativeQuery = true)
	PeminjamanEntity findPinjamByKode(String kodePeminjaman);
	@Query (value = "select count(id_peminjaman) from tabel_peminjaman where keterangan = 'Belum Dikembalikan'",nativeQuery = true)
	Integer FindTotalBukuPinjam();
	@Query ( value = "SELECT username AS Mode FROM tabel_peminjaman GROUP BY username ORDER BY COUNT(*) DESC limit 2", nativeQuery = true)
	List<String> FindMemberTerbaik();
	@Query (value = "select * from tabel_peminjaman where keterangan = 'Belum Dikembalikan'", nativeQuery = true)
	List<PeminjamanEntity> findAllBelumKembali();

}
