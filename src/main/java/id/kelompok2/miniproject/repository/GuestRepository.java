package id.kelompok2.miniproject.repository;

import id.kelompok2.miniproject.model.entity.BukuEntity;
import id.kelompok2.miniproject.model.entity.GuestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface GuestRepository extends JpaRepository<GuestEntity, Integer> {
    @Query(value = "SELECT * FROM tabel_guest WHERE tanggal_kehadiran = CURRENT_DATE", nativeQuery = true)
    List<GuestEntity> findByTanggalKehadiran();

    @Query(value = "SELECT * FROM tabel_guest WHERE date_part('month', (SELECT tanggal_kehadiran)) = date_part('month', (SELECT CURRENT_DATE)) AND date_part('year', (SELECT tanggal_kehadiran)) = date_part('year', (SELECT CURRENT_DATE))", nativeQuery = true)
    List<GuestEntity> findByTanggalKehadiranBulanIni();

    @Query(value = "SELECT count(id_guest) FROM tabel_guest WHERE date_part('month', (SELECT tanggal_kehadiran)) = date_part('month', (SELECT CURRENT_DATE)) AND date_part('year', (SELECT tanggal_kehadiran)) = date_part('year', (SELECT CURRENT_DATE))",nativeQuery = true)
    Integer findTotalGuestBulanIni();
}
