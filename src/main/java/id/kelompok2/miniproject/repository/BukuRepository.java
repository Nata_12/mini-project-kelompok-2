package id.kelompok2.miniproject.repository;

import id.kelompok2.miniproject.model.entity.BukuEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BukuRepository extends JpaRepository<BukuEntity, Integer> {
    @Query (value = "select * from public.tabel_buku where judul_buku = ?1", nativeQuery = true)
    BukuEntity findAllByJudulBukuLike(String judulBuku);

    @Query (value = "select id_buku from tabel_buku where judul_buku = ?", nativeQuery = true)
    Integer findIdByJudulbuku(String judulBuku);

    List<BukuEntity> findByJudulBukuStartsWithIgnoreCase (String judulBuku);

    @Query (value = "SELECT * FROM tabel_buku WHERE date_part('month', (SELECT tanggal_masuk)) = date_part('month', (SELECT CURRENT_DATE)) \n" +
            "AND date_part('year', (SELECT tanggal_masuk)) = date_part('year', (SELECT CURRENT_DATE))", nativeQuery = true)
    List<BukuEntity> findByTanggalMasuk();

    @Query (value = "select count(id_buku) from tabel_buku", nativeQuery = true)
    Integer findTotalBuku();

    List<BukuEntity> findByAuthorEntityIdPengarang(Integer idPengarang);

    List<BukuEntity> findByPublisherEntityIdPenerbit(Integer idPenerbit);

    @Query (value = "select * FROM tabel_buku where cover is not null order by tabel_buku.tahun_terbit desc limit 6", nativeQuery = true)
    List<BukuEntity> findByTahunTerbit ();
}
