package id.kelompok2.miniproject.repository;

import id.kelompok2.miniproject.model.entity.VoteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository extends JpaRepository<VoteEntity,Integer> {
    @Query (value = "select * from tabel_vote where username = ?1 and id_request = ?2", nativeQuery = true)
    VoteEntity findByUsername (String username, Integer id);
}
