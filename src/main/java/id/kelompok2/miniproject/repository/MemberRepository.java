package id.kelompok2.miniproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.kelompok2.miniproject.model.entity.MemberEntity;


@Repository
public interface MemberRepository extends JpaRepository<MemberEntity, Integer>{

	@Query (value = "select * from public.tabel_member where username = ?1", nativeQuery = true)
	public MemberEntity findIdByUname(String username);

	@Query (value = "select * from public.tabel_member where password = ?1", nativeQuery = true)
	public MemberEntity findIdByPassword(String password);

	List<MemberEntity> findByUsername (String username);
	List<MemberEntity> findAllByNoIdentitas (String noIdentitas);
	List<MemberEntity> findByEmail (String email);
	List<MemberEntity> findByPassword (String password);

	@Query (value = "select count(id_user) from tabel_member" ,nativeQuery = true)
	Integer findTotalMember();

	@Query (value = "select id_user from tabel_member where username = ?1", nativeQuery = true)
	Integer FindIdByUsername(String username);


}
