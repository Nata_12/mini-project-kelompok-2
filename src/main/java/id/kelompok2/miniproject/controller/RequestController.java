package id.kelompok2.miniproject.controller;

import id.kelompok2.miniproject.assembler.RequestAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.InputRequestDto;
import id.kelompok2.miniproject.model.dto.RequestDto;
import id.kelompok2.miniproject.model.dto.VoteDto;
import id.kelompok2.miniproject.model.entity.RequestEntity;
import id.kelompok2.miniproject.model.entity.VoteEntity;
import id.kelompok2.miniproject.repository.RequestRepository;
import id.kelompok2.miniproject.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/request")
@CrossOrigin(origins = "http://localhost:3000")
public class RequestController {

    @Autowired
    private RequestAssembler requestAssembler;

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private VoteRepository voteRepository;

    @GetMapping("/get")
    public DefaultResponse get() {
        List<RequestEntity> entityList = requestRepository.findAll();
        List<RequestDto> dtoList = entityList.stream().map(entity -> requestAssembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(dtoList);
    }

    @GetMapping("/get/terbaik")
    public DefaultResponse getBest() {
        List<RequestEntity> entityList = requestRepository.FindTerbaik();
        List<RequestDto> dtoList = entityList.stream().map(entity -> requestAssembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(dtoList);
    }

    @PostMapping("/post")
    public DefaultResponse post(@RequestBody InputRequestDto inputRequestDto) {
        RequestDto dto = new RequestDto();
        dto.setJudulBuku(inputRequestDto.getJudulBuku());
        dto.setNamaPengarang(inputRequestDto.getNamaPengarang());
        dto.setJumlahVote(0);

        RequestEntity entity = requestAssembler.fromDto(dto);
        requestRepository.save(entity);
        return DefaultResponse.ok(requestAssembler.fromEntity(entity));
    }

    @PostMapping("/vote")
    public String post(@RequestBody VoteDto dto) {
        VoteEntity entity = new VoteEntity();
        RequestEntity requestEntity = requestRepository.FindByIdRequest(dto.getIdRequest());
        entity.setUsername(dto.getUsername());
        entity.setRequestEntity(requestEntity);

        if (voteRepository.findByUsername(dto.getUsername(),dto.getIdRequest()) == null) {
            voteRepository.save(entity);
            requestEntity.setJumlahVote(requestEntity.getJumlahVote()+1);
            requestRepository.save(requestEntity);
            return "berhasil";
        }

        return "gagal";
    }

//    @PostMapping("/vote")
//    public DefaultResponse vote(@RequestBody InputRequestDto inputRequestDto) {
//        RequestDto dto = new RequestDto();
//        dto.setJudulBuku(inputRequestDto.getJudulBuku());
//        dto.setNamaPengarang(inputRequestDto.getNamaPengarang());
//        dto.setIdRequest(requestRepository.FindIdByJudulBuku(inputRequestDto.getJudulBuku()));
//        dto.setJumlahVote(requestRepository.FindJumlahVote(requestRepository.FindIdByJudulBuku(inputRequestDto.getJudulBuku())));
//
//        RequestEntity entity = requestAssembler.fromDto(dto);
//        requestRepository.save(entity);
//        return DefaultResponse.ok(requestAssembler.fromEntity(entity));
//
//
//    }
}
