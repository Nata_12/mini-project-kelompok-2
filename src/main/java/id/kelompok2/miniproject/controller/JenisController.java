package id.kelompok2.miniproject.controller;

import id.kelompok2.miniproject.assembler.JenisAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.JenisDto;
import id.kelompok2.miniproject.model.entity.JenisEntity;
import id.kelompok2.miniproject.repository.JenisRepository;
import id.kelompok2.miniproject.repository.LemariRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/jenis")
@CrossOrigin(origins = "http://localhost:3000")
public class JenisController {
    @Autowired
    JenisRepository repository;
    @Autowired
    JenisAssembler assembler;

    @GetMapping("/semuajenis")
    public DefaultResponse get(){
        List<JenisEntity> jenisEntityList = repository.findAll();
        List<JenisDto> jenisDtoList = jenisEntityList.stream().map(entity -> assembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(jenisDtoList);
    }

    @PostMapping
    public DefaultResponse insert(@RequestBody JenisDto dto){
        JenisEntity entity = assembler.fromDto(dto);
        repository.save(entity);
        return DefaultResponse.ok(assembler.fromEntity(entity));
    }

}
