package id.kelompok2.miniproject.controller;

import java.util.List;
import java.util.stream.Collectors;

import id.kelompok2.miniproject.model.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import id.kelompok2.miniproject.assembler.MemberAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.entity.MemberEntity;
import id.kelompok2.miniproject.repository.MemberRepository;


@RestController
@RequestMapping("/member")
@CrossOrigin(origins = "http://localhost:3000")


public class MemberController {
	
	@Autowired
	private MemberRepository memberRepository;
	
	@Autowired
	private MemberAssembler memberAssembler;
	
	
	// http://localhost:9191/v1/app/member
	@GetMapping("/all")
	 public DefaultResponse getAll() {
        List<MemberEntity> memberList = memberRepository.findAll();
        List<MemberDto> memberDtos = memberList.stream().map( member-> memberAssembler.fromEntity(member))
                .collect(Collectors.toList());
        return DefaultResponse.ok(memberDtos);
    }
	
	 @GetMapping("/{id}")
	    public DefaultResponse get(@PathVariable Integer id) {
	        MemberDto memberDto = memberAssembler.fromEntity(memberRepository.findById(id).get());
	        return DefaultResponse.ok(memberDto);
	    }

	 @GetMapping("/id/{username}")
	    public DefaultResponse getUsername(@PathVariable String username) {
		if (memberRepository.findByUsername(username).isEmpty()==false){
			List<MemberEntity> memberEntities = memberRepository.findByUsername(username);
			List<MemberDto> memberDtos = memberEntities.stream().map(memberEntity -> memberAssembler.fromEntity(memberEntity))
					.collect(Collectors.toList());
			return DefaultResponse.ok(memberDtos);
		} else {
			return DefaultResponse.error("Username Tidak Ditemukan");
			}
	    }


	@PostMapping("/update/{id}")
	public DefaultResponse updateData (@RequestBody UpdateMemberDto updateMemberDto) {
		MemberDto dto = new MemberDto();
//		dto.setUsername(updateMemberDto.getUsername());
//		dto.setPassword(updateMemberDto.getPassword());
//		dto.setNamaLengkap(updateMemberDto.getNamaLengkap());
//		dto.setEmail(updateMemberDto.getEmail());
//		dto.setAlamat(updateMemberDto.getAlamat());
//		dto.setPekerjaan(updateMemberDto.getPekerjaan());
//		dto.setNoIdentitas(updateMemberDto.getNoIdentitas());
//		dto.setFoto(updateMemberDto.getFoto());
//
//		MemberEntity memberEntity = memberAssembler.fromDto(dto);
//		memberEntity.setIdUser(updateMemberDto.getIdUser());
//
//		if (memberRepository.findAllByNoIdentitas(updateMemberDto.getNoIdentitas()).isEmpty()==true
//				&& memberRepository.findByUsername(updateMemberDto.getUsername()).isEmpty()== true
//				&& memberRepository.findByEmail(updateMemberDto.getEmail()).isEmpty()== true
//				&& memberRepository.findByPassword(updateMemberDto.getPassword()).isEmpty()==true) {
//			memberRepository.save(memberEntity);
//			return DefaultResponse.ok(memberAssembler.fromEntity(memberEntity));
//		} else if (memberRepository.findByUsername(updateMemberDto.getUsername()).isEmpty() == false
//				&& memberRepository.findAllByNoIdentitas(updateMemberDto.getNoIdentitas()).isEmpty() == true
//				&& memberRepository.findByEmail(updateMemberDto.getEmail()).isEmpty() == true
//				&& memberRepository.findByPassword(updateMemberDto.getPassword()).isEmpty()==true) {
//			return DefaultResponse.error("Update profil gagal, username sudah terdaftar");
//		} else if (memberRepository.findByEmail(updateMemberDto.getEmail()).isEmpty() == false
//				&& memberRepository.findAllByNoIdentitas(updateMemberDto.getNoIdentitas()).isEmpty()== true
//				&& memberRepository.findByUsername(updateMemberDto.getUsername()).isEmpty()==true
//				&& memberRepository.findByPassword(updateMemberDto.getPassword()).isEmpty()==true) {
//			return DefaultResponse.error("Update profil gagal, email sudah terdaftar");
//		} else if (memberRepository.findAllByNoIdentitas(updateMemberDto.getNoIdentitas()).isEmpty() == false
//				&& memberRepository.findByUsername(updateMemberDto.getUsername()).isEmpty()==true
//				&& memberRepository.findByEmail(updateMemberDto.getEmail()).isEmpty()==true
//				&& memberRepository.findByPassword(updateMemberDto.getPassword()).isEmpty()==true) {
//			return DefaultResponse.error("Update profil gagal, nomor identitas sudah terdaftar");
//		} else if (memberRepository.findAllByNoIdentitas(updateMemberDto.getNoIdentitas()).isEmpty() == true
//				&& memberRepository.findByUsername(updateMemberDto.getUsername()).isEmpty()==true
//				&& memberRepository.findByEmail(updateMemberDto.getEmail()).isEmpty()==true
//				&& memberRepository.findByPassword(updateMemberDto.getPassword()).isEmpty()== false) {
//			return DefaultResponse.error("Update profil gagal, password sudah terdaftar");
//		} else {
//			return DefaultResponse.error("Update profil gagal, periksa kembali data Anda");
//		}

		dto.setUsername(updateMemberDto.getUsername());
		dto.setPassword(updateMemberDto.getPassword());
		dto.setNamaLengkap(updateMemberDto.getNamaLengkap());
		dto.setEmail(updateMemberDto.getEmail());
		dto.setAlamat(updateMemberDto.getAlamat());
		dto.setPekerjaan(updateMemberDto.getPekerjaan());
		dto.setNoIdentitas(updateMemberDto.getNoIdentitas());
		dto.setFoto(updateMemberDto.getFoto());

		MemberEntity memberEntity = memberAssembler.fromDto(dto);
		memberEntity.setIdUser(updateMemberDto.getIdUser());
		memberRepository.save(memberEntity);
		return DefaultResponse.ok(memberAssembler.fromEntity(memberEntity));

	}

	 @PostMapping
	    public DefaultResponse insert(@RequestBody MemberDto dto) {
		 MemberEntity memberEntity = memberAssembler.fromDto(dto);
		 if (memberRepository.findAllByNoIdentitas(dto.getNoIdentitas()).isEmpty()==true
	        		&& memberRepository.findByUsername(dto.getUsername()).isEmpty()== true
	        		&& memberRepository.findByEmail(dto.getEmail()).isEmpty()== true
					&& memberRepository.findByPassword(dto.getPassword()).isEmpty()==true) {
			 memberRepository.save(memberEntity);
			 return DefaultResponse.ok(memberAssembler.fromEntity(memberEntity));
		 } else if (memberRepository.findByUsername(dto.getUsername()).isEmpty() == false
		 			&& memberRepository.findAllByNoIdentitas(dto.getNoIdentitas()).isEmpty() == true
		 			&& memberRepository.findByEmail(dto.getEmail()).isEmpty() == true) {
			 return DefaultResponse.error("Registrasi gagal, username sudah terdaftar");
		 } else if (memberRepository.findByEmail(dto.getEmail()).isEmpty() == false
		 			&& memberRepository.findAllByNoIdentitas(dto.getNoIdentitas()).isEmpty()== true
		 			&& memberRepository.findByUsername(dto.getUsername()).isEmpty()==true) {
			 return DefaultResponse.error("Registrasi gagal, email sudah terdaftar");
		 } else if (memberRepository.findAllByNoIdentitas(dto.getNoIdentitas()).isEmpty() == false
		 			&& memberRepository.findByUsername(dto.getUsername()).isEmpty()==true
		 			&& memberRepository.findByEmail(dto.getEmail()).isEmpty()==true) {
			 return DefaultResponse.error("Registrasi gagal, nomor identitas sudah terdaftar");
		 } else {
		 	return DefaultResponse.error("Registrasi gagal, periksa kembali data Anda");
		 }
	       
	    }

	  @PostMapping("/login")
	  public DefaultResponse insertLogin (@RequestBody LoginDto loginDto) {

		  if (loginDto.getUsername().equals("admin")) {
			  if (loginDto.getPassword().equals("adminaja")) {
				  return DefaultResponse.ok("Login berhasil, Admin!!");
			  } else {
				  return DefaultResponse.error("Login gagal, periksa kembali password Anda");
			  }
		  } else if (memberRepository.findIdByUname(loginDto.getUsername()) != null
				  && memberRepository.findIdByUname(loginDto.getUsername()).getPassword().equals(loginDto.getPassword()) == true) {
			  return DefaultResponse.ok("Selamat, Anda berhasil masuk");
		  } else if (memberRepository.findIdByUname(loginDto.getUsername()) != null
				  && memberRepository.findIdByUname(loginDto.getUsername()).getPassword().equals(loginDto.getPassword()) == false) {
			  return DefaultResponse.error("Login gagal, periksa kembali password Anda");
		  } else if (memberRepository.findIdByUname(loginDto.getUsername()) == null) {
			  return DefaultResponse.error("Login gagal, periksa kembali username Anda");
		  } else {
			  return DefaultResponse.error("Login gagal, periksa kembali data Anda");
		  }
	  }

	 
	 @PutMapping("/{id}")
	 	public DefaultResponse update(@RequestBody MemberDto memberDto, @PathVariable Integer id) {
		 	MemberEntity memberEntity = memberAssembler.fromDto(memberDto);
		 	memberEntity.setIdUser(id);
		 	memberRepository.save(memberEntity);
		 	return DefaultResponse.ok(memberAssembler.fromEntity(memberEntity));
		 
	 }

	 @GetMapping("/totalmember")
	public Integer getTotalMember() {
		return memberRepository.findTotalMember();
	 }

	
	

}
