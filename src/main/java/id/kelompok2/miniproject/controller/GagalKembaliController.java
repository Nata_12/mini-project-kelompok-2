package id.kelompok2.miniproject.controller;

//import java.util.List;
//import java.util.stream.Collectors;

//import id.kelompok2.miniproject.model.dto.PengembalianRusakDto;
//import id.kelompok2.miniproject.repository.BukuRepository;
//import id.kelompok2.miniproject.repository.MemberRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
import id.kelompok2.miniproject.assembler.GagalKembaliAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.GagalKembaliDto;
import id.kelompok2.miniproject.model.entity.GagalKembaliEntity;
import id.kelompok2.miniproject.repository.GagalKembaliRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

//import id.kelompok2.miniproject.assembler.GagalKembaliAssembler;
//import id.kelompok2.miniproject.configuration.DefaultResponse;
//import id.kelompok2.miniproject.model.dto.GagalKembaliDto;
//import id.kelompok2.miniproject.model.entity.GagalKembaliEntity;
//import id.kelompok2.miniproject.repository.GagalKembaliRepository;
//import id.kelompok2.miniproject.service.GagalKembaliService;

@RestController
@RequestMapping("/gagalkembali")
@CrossOrigin(origins = "http://localhost:3000")
public class GagalKembaliController {
	@Autowired
	private GagalKembaliRepository repository;
	@Autowired
	private GagalKembaliAssembler assembler;
//	@Autowired
//	private GagalKembaliService service;
//	@Autowired
//	private BukuRepository bukuRepository;
//	@Autowired
//	private MemberRepository memberRepository;

	@GetMapping
	public DefaultResponse getData(){
		List<GagalKembaliEntity> kembaliList = repository.findAll();
		List<GagalKembaliDto> kembaliDto = kembaliList.stream().map(gagalKembali -> assembler.fromEntity(gagalKembali))
				.collect(Collectors.toList());
		return DefaultResponse.ok(kembaliDto);
	}

	@GetMapping("/bukurusak")
	public DefaultResponse getDataBukuRusak(){
		List<GagalKembaliEntity> kembaliList = repository.FindBukuRusak();
		List<GagalKembaliDto> kembaliDto = kembaliList.stream().map(gagalKembali -> assembler.fromEntity(gagalKembali))
				.collect(Collectors.toList());
		return DefaultResponse.ok(kembaliDto);
	}

	@GetMapping("/bukuhilang")
	public DefaultResponse getDataBukuHilang(){
		List<GagalKembaliEntity> kembaliList = repository.FindBukuHilang();
		List<GagalKembaliDto> kembaliDto = kembaliList.stream().map(gagalKembali -> assembler.fromEntity(gagalKembali))
				.collect(Collectors.toList());
		return DefaultResponse.ok(kembaliDto);
	}

	@GetMapping("/rusak")
	public Integer getTotalBukuRusak() {
		return repository.FindTotalBukuRusak();
	}

	@GetMapping("/hilang")
	public Integer getTotalBukuHilang() {
		return repository.FindTotalBukuHilang();
	}
	
//	@PostMapping("/tambah")
//	public DefaultResponse insertGagal(@RequestBody PengembalianRusakDto dto) {
//		GagalKembaliDto gagalKembaliDto = new GagalKembaliDto();
//		gagalKembaliDto.setJudulBuku(dto.getJudulBuku());
//		gagalKembaliDto.setIdBuku(bukuRepository.findIdByJudulbuku(dto.getJudulBuku()));
//		gagalKembaliDto.setIdUser(memberRepository.FindIdByUsername(dto.getUsername()));
//		gagalKembaliDto.setKeterangan(dto.getKeterangan());
//
//		GagalKembaliEntity entity = assembler.fromDto(gagalKembaliDto);
//		repository.save(entity);
//
//		return DefaultResponse.ok(assembler.fromEntity(entity));
//	}
}
