package id.kelompok2.miniproject.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.kelompok2.miniproject.assembler.PublisherAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.PublisherDto;
import id.kelompok2.miniproject.model.entity.PublisherEntity;
import id.kelompok2.miniproject.repository.PublisherRepository;

@RestController
@RequestMapping("/publisher")
@CrossOrigin(origins = "http://localhost:3000")
public class PublisherController {
	@Autowired
	private PublisherRepository repository;
	@Autowired
	private PublisherAssembler assembler;

	@GetMapping
	public DefaultResponse get() {
		List<PublisherEntity> publisherEntities = repository.findAll();
		List<PublisherDto> publisherDtos = publisherEntities.stream().map(penerbit -> assembler.fromEntity(penerbit))
				.collect(Collectors.toList());
		return DefaultResponse.ok(publisherDtos);
	}

	@GetMapping("/{name}")
	public DefaultResponse getByName(@PathVariable String name) {
		if (repository.findByNamaPenerbitStartsWithIgnoreCase(name).isEmpty() == false) {
			List<PublisherEntity> publisherEntities = repository.findByNamaPenerbitStartsWithIgnoreCase(name);
			List<PublisherDto> publisherDtos = publisherEntities.stream().map(entity -> assembler.fromEntity(entity))
					.collect(Collectors.toList());
			return DefaultResponse.ok(publisherDtos);
		} else {
			return DefaultResponse.error("Penerbit Tidak Ditemukan");
		}
	}

	@PostMapping("/insert")
	public DefaultResponse insert(@RequestBody PublisherDto dto) {
		PublisherEntity publisherEntity = assembler.fromDto(dto);
		repository.save(publisherEntity);
		return DefaultResponse.ok(assembler.fromEntity(publisherEntity));
	}

	@PostMapping("/insert2")
	public DefaultResponse insert2(@RequestBody PublisherDto dto) {
		PublisherEntity publisherEntity = assembler.fromDto(dto);
		if (repository.findByNamaPenerbit(dto.getNamaPenerbit()).isEmpty() == true) {
			repository.save(publisherEntity);
			return DefaultResponse.ok(assembler.fromEntity(publisherEntity));				
		} else {
			return DefaultResponse.error("Nama Penerbit telah ada pada data penerbit.");
		}
	}

	@PutMapping("/update/{idPenerbit}")
	public DefaultResponse insert(@RequestBody PublisherDto dto, @PathVariable Integer idPenerbit) {
		PublisherEntity publisherEntity = assembler.fromDto(dto);
		publisherEntity.setIdPenerbit(idPenerbit);
		repository.save(publisherEntity);
		return DefaultResponse.ok(assembler.fromEntity(publisherEntity));
	}

	@DeleteMapping("/delete/{idPenerbit}")
	public void delete(@PathVariable Integer idPenerbit) {
		repository.deleteById(idPenerbit);
	}
}
