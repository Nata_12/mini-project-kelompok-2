package id.kelompok2.miniproject.service;

import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.PeminjamanDto;


public interface PeminjamanService {
	PeminjamanDto insertDataPeminjaman(PeminjamanDto dto);
	DefaultResponse deleteDataPeminjaman(Integer idUser);
}

