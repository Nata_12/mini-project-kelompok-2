package id.kelompok2.miniproject.service;


import javax.transaction.Transactional;

import id.kelompok2.miniproject.assembler.GagalKembaliAssembler;
import id.kelompok2.miniproject.model.dto.GagalKembaliDto;
import id.kelompok2.miniproject.model.dto.PengembalianRusakDto;
import id.kelompok2.miniproject.model.entity.GagalKembaliEntity;
import id.kelompok2.miniproject.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.kelompok2.miniproject.assembler.PengembalianAssembler;
import id.kelompok2.miniproject.model.dto.PengembalianDto;
import id.kelompok2.miniproject.model.entity.BukuEntity;
import id.kelompok2.miniproject.model.entity.PeminjamanEntity;
import id.kelompok2.miniproject.model.entity.PengembalianEntity;

@Service
@Transactional
public class PengembalianServiceImpl implements PengembalianService {

	@Autowired
	private PeminjamanRepository pinjamRepository;
	@Autowired
	private PengembalianAssembler assembler;
	@Autowired
	private PengembalianRepository repository;
	@Autowired
	private BukuRepository bukuRepository;
	@Autowired
	private GagalKembaliAssembler gagalKembaliAssembler;
	@Autowired
	private GagalKembaliRepository gagalKembaliRepository;

	@Override
	public PengembalianDto insertDataPengembalian(String kodePeminjaman, PengembalianDto dto) {
		PeminjamanEntity pinjam = pinjamRepository.findAllByUsernameLike(kodePeminjaman);
		PengembalianEntity entity = assembler.fromDto(dto);
		entity.setJudulBuku(pinjam.getJudulBuku());
		entity.setUsername(pinjam.getUsername());
		entity.setIdPeminjaman(pinjam);
		repository.save(entity);
		pinjamRepository.save(updateKeteranganPinjam(kodePeminjaman));
		bukuRepository.save(updateStokBuku(entity.getIdPeminjaman().getJudulBuku()));
		return assembler.fromEntity(entity);
	}

	@Override
	public GagalKembaliDto insertDataRusak(String kodePeminjaman, PengembalianRusakDto pengembalianRusakDto) {
		PeminjamanEntity pinjam = pinjamRepository.findAllByUsernameLike(kodePeminjaman);
		GagalKembaliDto dto = new GagalKembaliDto();
		dto.setKodePeminjaman(pengembalianRusakDto.getKodePeminjaman());
		dto.setKeterangan(pengembalianRusakDto.getKeterangan());
		dto.setJudulBuku(pinjam.getJudulBuku());
		dto.setUsername(pinjam.getUsername());
		GagalKembaliEntity entity = gagalKembaliAssembler.fromDto(dto);
		entity.setIdPeminjaman(pinjam);
		gagalKembaliRepository.save(entity);
		pinjamRepository.save(updateKeteranganPinjam(kodePeminjaman));
//		bukuRepository.save(updateStokBuku(dto));
		return dto;
	}
	
	public PeminjamanEntity updateKeteranganPinjam(String kodePeminjaman) {
		PeminjamanEntity pinjam = pinjamRepository.findAllByUsernameLike(kodePeminjaman);
		pinjam.setKeteranganPinjam("Sudah Dikembalikan");
		return pinjam;
	}
	
	public BukuEntity updateStokBuku(String judulBuku) {
		BukuEntity buku = bukuRepository.findAllByJudulBukuLike(judulBuku);
		buku.setStok(buku.getStok()+1);
		return buku;
	}
	
}
