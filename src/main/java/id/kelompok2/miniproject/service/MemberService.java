package id.kelompok2.miniproject.service;

import id.kelompok2.miniproject.model.dto.MemberDto;

public interface MemberService {
	MemberDto insertMember(MemberDto memberDto);
}
