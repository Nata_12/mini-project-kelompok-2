package id.kelompok2.miniproject.service;

import id.kelompok2.miniproject.model.dto.GagalKembaliDto;

public interface GagalKembaliService {
	GagalKembaliDto insertBukuKembali(GagalKembaliDto dto);
}
