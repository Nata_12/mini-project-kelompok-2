package id.kelompok2.miniproject.service;

import id.kelompok2.miniproject.model.dto.GagalKembaliDto;
import id.kelompok2.miniproject.model.dto.PengembalianDto;
import id.kelompok2.miniproject.model.dto.PengembalianRusakDto;

public interface PengembalianService {
	public PengembalianDto insertDataPengembalian(String kodePeminjaman, PengembalianDto dto);
	public GagalKembaliDto insertDataRusak(String kodePeminjaman, PengembalianRusakDto pengembalianRusakDto);
}
