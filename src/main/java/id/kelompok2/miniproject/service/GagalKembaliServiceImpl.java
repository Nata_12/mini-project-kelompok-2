package id.kelompok2.miniproject.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.kelompok2.miniproject.assembler.GagalKembaliAssembler;
import id.kelompok2.miniproject.model.dto.GagalKembaliDto;
import id.kelompok2.miniproject.model.entity.GagalKembaliEntity;
import id.kelompok2.miniproject.repository.GagalKembaliRepository;
import id.kelompok2.miniproject.repository.MemberRepository;

@Service
@Transactional
public class GagalKembaliServiceImpl implements GagalKembaliService{
	@Autowired
	private GagalKembaliRepository repository;
	@Autowired
	private GagalKembaliAssembler assembler;
	
	@Override
	public GagalKembaliDto insertBukuKembali(GagalKembaliDto dto) {
		GagalKembaliEntity entity = assembler.fromDto(dto);
		repository.save(entity);
		return assembler.fromEntity(entity);
	}
	
}
