package id.kelompok2.miniproject.service;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.kelompok2.miniproject.assembler.PeminjamanAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.PeminjamanDto;
import id.kelompok2.miniproject.model.entity.BukuEntity;
import id.kelompok2.miniproject.model.entity.MemberEntity;
import id.kelompok2.miniproject.model.entity.PeminjamanEntity;
import id.kelompok2.miniproject.repository.BukuRepository;
import id.kelompok2.miniproject.repository.MemberRepository;
import id.kelompok2.miniproject.repository.PeminjamanRepository;

@Service
@Transactional
public class PeminjamanServiceImpl implements PeminjamanService {
	@Autowired
	private PeminjamanRepository repository;
	@Autowired
	private PeminjamanAssembler assembler;
	@Autowired 
	private MemberRepository memberRepository;
	@Autowired
	private BukuRepository bukuRepository;

	@Override
	public PeminjamanDto insertDataPeminjaman(PeminjamanDto dto) {
		MemberEntity member = memberRepository.findIdByUname(dto.getUsername());
		PeminjamanEntity entity = assembler.fromDto(dto);
		entity.setUsername(dto.getUsername());
		entity.setIdUser(member);
		repository.save(entity);
		entity.setKodePeminjaman("PJ-" + entity.getIdPeminjaman());
		repository.save(entity);
		bukuRepository.save(updateStokBuku(dto));
		return assembler.fromEntity(entity);
	}

	@Override
	public DefaultResponse deleteDataPeminjaman(Integer idPeminjaman) {	
		repository.deleteById(idPeminjaman);
		return null;
		
	}
	
	public BukuEntity updateStokBuku(PeminjamanDto dto) {
		BukuEntity buku = bukuRepository.findAllByJudulBukuLike(dto.getJudulBuku());
		buku.setStok(buku.getStok()-1);
		return buku;
	}

}
