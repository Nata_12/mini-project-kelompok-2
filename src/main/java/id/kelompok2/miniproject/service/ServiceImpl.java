package id.kelompok2.miniproject.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.kelompok2.miniproject.assembler.MemberAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.MemberDto;
import id.kelompok2.miniproject.model.entity.MemberEntity;
import id.kelompok2.miniproject.repository.MemberRepository;

@Service
@Transactional

public class ServiceImpl implements MemberService {
	
	@Autowired
	private MemberRepository memberRepository;
	
	@Autowired
	private MemberAssembler memberAssembler ;

	@Override
	public MemberDto insertMember(MemberDto memberDto) {
		MemberEntity memberEntity = memberAssembler.fromDto(memberDto);

//		MemberEntity memberEntity = memberRepository.findByNoIdentitas(No);
		if (memberRepository.findAllByNoIdentitas(memberDto.getNoIdentitas()).isEmpty()==true) {
			 memberRepository.save(memberEntity);
		}
		return null;
	}

	
}
