package id.kelompok2.miniproject.model.entity;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table (name="tabel_gagal_kembali")
public class GagalKembaliEntity {
	@Id 
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name="id_buku_rusak")
	private Integer idBukuRusak;
	@Column (name="judul_buku")
	private String judulBuku;
	@Column (name="keterangan")
	private String keterangan;
	@Column (name="username")
	private String username;
	@Column (name="kode_peminjaman")
	private String kodePeminjaman;
	@ManyToOne
	@JoinColumn (name="id_peminjaman")
	private PeminjamanEntity idPeminjaman;
//	@ManyToOne
//	@JoinColumn (name="id_user")
//	private MemberEntity idUser;
	// @Column (name="id_user")
	// private Integer idUser;
}
