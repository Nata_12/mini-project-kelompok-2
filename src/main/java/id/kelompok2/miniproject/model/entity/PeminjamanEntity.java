package id.kelompok2.miniproject.model.entity;

import java.sql.Date;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table (name="tabel_peminjaman")
public class PeminjamanEntity {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name="id_peminjaman", unique = true )
	private Integer idPeminjaman;
	@Column (name="judul_buku")
	private String judulBuku;
	@Column (name="tanggal_peminjaman")
	private Date tanggalPeminjaman;
	@Column (name="tanggal_pengembalian")
	private Date tanggalPengembalian;
	@Column (name = "username")
	private String username;
	@ManyToOne
	@JoinColumn (name = "id_user")
	private MemberEntity idUser;
	@Column (name = "keterangan")
	private String keteranganPinjam;
	@Column (name = "kode_peminjaman", unique = true)
	private String kodePeminjaman;
}
