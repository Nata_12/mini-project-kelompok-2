package id.kelompok2.miniproject.model.entity;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "tabel_member")

public class MemberEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_user", length = 25)
	private Integer idUser;
	
	@Column(name = "username", length = 255, nullable = false, unique = true , updatable = false)
	private String username;
	
	@Column(name = "password", length = 25, nullable = false)
	private String password;
	
	@Column(name = "nama_lengkap", length = 255, nullable = false)
	private String namaLengkap;
	
	@Column(name = "email", length = 255, nullable = false, unique = true, updatable = false)
	private String email;
	
	@Column(name = "alamat", length = 255, nullable = false)
	private String alamat;
	
	@Column(name = "pekerjaan", length = 50, nullable = false)
	private String pekerjaan;
	
	@Column(name = "no_identitas", length = 25, nullable = false, unique = true, updatable = false)
	private String noIdentitas;

	@Column(name = "foto")
	private String foto;


}
