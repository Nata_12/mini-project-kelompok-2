package id.kelompok2.miniproject.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tabel_request")
public class RequestEntity {
    @Id
    @GeneratedValue (strategy =  GenerationType.IDENTITY)
    @Column(name = "id_request")
    private Integer idRequest;

    @Column(name = "judul_buku")
    private String judulBuku;

    @Column(name = "nama_pengarang")
    private String namaPengarang;

    @Column(name = "jumlah_vote")
    private Integer jumlahVote;
}
