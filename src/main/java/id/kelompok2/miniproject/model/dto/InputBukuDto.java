package id.kelompok2.miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InputBukuDto {
    private Integer idBuku;
    private String judulBuku;
    private String cover;
    private String namaPengarang;
    private String namaPenerbit;
    private Integer stok;
    private String tahunTerbit;
    private String namaJenis;
    private String kodeLemari;
    private String hargaBuku;
    private Date tanggalMasuk;

}
