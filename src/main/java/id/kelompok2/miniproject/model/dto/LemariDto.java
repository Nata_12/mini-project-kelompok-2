package id.kelompok2.miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LemariDto {
    private Integer idLemari;
    private String kodeLemari;
    private String lokasi;
    private Integer idJenis;
}
