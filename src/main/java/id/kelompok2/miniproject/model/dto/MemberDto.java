package id.kelompok2.miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class MemberDto {
	
	private Integer idUser;
	private String foto;
	private String username;
	private String password;
	private String namaLengkap;
	private String email;
	private String alamat;
	private String pekerjaan;
	private String noIdentitas;

	

}
