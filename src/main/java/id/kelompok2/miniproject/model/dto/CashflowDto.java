package id.kelompok2.miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CashflowDto {
    private Integer idTransaksi;
    private  Integer jumlahSaldo;
    private String keterangan;
    private String totalPemasukan;

}
