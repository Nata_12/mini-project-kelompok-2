package id.kelompok2.miniproject.model.dto;

import lombok.Data;

@Data
public class PengembalianRusakDto {
    private String username;
    private String judulBuku;
    private String keterangan;
    private String kodePeminjaman;
}
