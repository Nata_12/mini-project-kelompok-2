package id.kelompok2.miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GagalKembaliDto {
	private Integer idBukuRusak;
	private String kodePeminjaman;
	private String username;
	private String judulBuku;
	private String keterangan;
}
