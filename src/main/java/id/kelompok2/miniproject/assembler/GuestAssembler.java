package id.kelompok2.miniproject.assembler;

import id.kelompok2.miniproject.model.dto.GuestDto;
import id.kelompok2.miniproject.model.entity.GuestEntity;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;

@Component
public class GuestAssembler implements InterfaceAssembler<GuestEntity, GuestDto> {

    @Override
    public GuestEntity fromDto(GuestDto dto) {
        if (dto == null)
        return null;

        GuestEntity entity = new GuestEntity();
        if (dto.getIdGuest() != null) entity.setIdGuest(dto.getIdGuest());
        if (dto.getNama() != null) entity.setNama(dto.getNama());
        if (dto.getTujuan() != null) entity.setTujuan(dto.getTujuan());
        LocalDate date = LocalDate.now();
        entity.setTanggalKehadiran(Date.valueOf(date));

        return entity;
    }

    @Override
    public GuestDto fromEntity(GuestEntity entity) {
        if (entity == null)
        return null;

        return GuestDto.builder()
                .idGuest(entity.getIdGuest())
                .nama(entity.getNama())
                .tujuan(entity.getTujuan())
                .tanggalKehadiran(entity.getTanggalKehadiran())
                .build();
    }
}
