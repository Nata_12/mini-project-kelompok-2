package id.kelompok2.miniproject.assembler;

import id.kelompok2.miniproject.model.entity.BukuEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.kelompok2.miniproject.model.dto.GagalKembaliDto;
import id.kelompok2.miniproject.model.entity.GagalKembaliEntity;
import id.kelompok2.miniproject.model.entity.PeminjamanEntity;
import id.kelompok2.miniproject.repository.GagalKembaliRepository;
import id.kelompok2.miniproject.repository.PeminjamanRepository;

@Component
public class GagalKembaliAssembler implements InterfaceAssembler<GagalKembaliEntity, GagalKembaliDto >{
	@Autowired
	private GagalKembaliRepository repository;
	@Autowired
	PeminjamanRepository pinjamRepository;

	@Override
	public GagalKembaliEntity fromDto(GagalKembaliDto dto) {
		if (dto == null) return null;
		PeminjamanEntity pinjam = pinjamRepository.findAllByUsernameLike(dto.getKodePeminjaman());
		GagalKembaliEntity entity = new GagalKembaliEntity();
		if (dto.getJudulBuku() != null) entity.setJudulBuku(dto.getJudulBuku());
		if (dto.getUsername() != null) entity.setUsername(dto.getUsername());
		if (dto.getKodePeminjaman() != null) entity.setKodePeminjaman(dto.getKodePeminjaman());
		if (dto.getKeterangan() != null) entity.setKeterangan(dto.getKeterangan());
		return entity;
	}

	@Override
	public GagalKembaliDto fromEntity(GagalKembaliEntity entity) {
		if(entity == null) return null;
		return GagalKembaliDto.builder()
				.idBukuRusak(entity.getIdBukuRusak())
				// .idUser(entity.getIdUser())
				.kodePeminjaman(entity.getIdPeminjaman().getKodePeminjaman())
				.username(entity.getIdPeminjaman().getUsername())
				.judulBuku(entity.getIdPeminjaman().getJudulBuku())
				.keterangan(entity.getKeterangan())
				.build();
	}
	
	
}
