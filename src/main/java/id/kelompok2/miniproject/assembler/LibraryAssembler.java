package id.kelompok2.miniproject.assembler;

import id.kelompok2.miniproject.model.dto.InputLemariDto;
import id.kelompok2.miniproject.model.dto.LemariDto;
import id.kelompok2.miniproject.model.entity.LemariEntity;
import org.springframework.stereotype.Component;

@Component
public class LibraryAssembler implements InterfaceAssembler<LemariEntity, InputLemariDto>{

    @Override
    public LemariEntity fromDto(InputLemariDto dto) {
        return null;
    }

    @Override
    public InputLemariDto fromEntity(LemariEntity entity) {
        if (entity == null)
            return null;

        return InputLemariDto.builder()
                .idLemari(entity.getIdLemari())
                .kodeLemari(entity.getKodeLemari())
                .lokasi(entity.getLokasi())
                .jenisBuku(entity.getJenisEntity().getNamaJenis())
                .build();
    }
}
