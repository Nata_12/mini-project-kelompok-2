package id.kelompok2.miniproject.assembler;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.kelompok2.miniproject.model.dto.MemberDto;
import id.kelompok2.miniproject.model.entity.MemberEntity;
import id.kelompok2.miniproject.repository.MemberRepository;

@Component

public class MemberAssembler implements InterfaceAssembler<MemberEntity, MemberDto> {

	@Autowired
	private MemberRepository memberRepository;

	@Override
	public MemberEntity fromDto(MemberDto dto) {
		if (dto == null)
		return null;
		
		MemberEntity memberEntity = new MemberEntity();
		if (dto.getIdUser() !=null) {
			Optional<MemberEntity> temp = this.memberRepository.findById(dto.getIdUser());
			if(temp.isPresent()) {
				memberEntity = temp.get();
			}
		}
		
		if (dto.getIdUser() != null) memberEntity.setIdUser(dto.getIdUser());
		if (dto.getFoto() !=null) memberEntity.setFoto(dto.getFoto());
		if (dto.getUsername() != null) memberEntity.setUsername(dto.getUsername());
		if (dto.getPassword() != null) memberEntity.setPassword(dto.getPassword());
		if (dto.getNamaLengkap() != null) memberEntity.setNamaLengkap(dto.getNamaLengkap());
		if (dto.getEmail() != null) memberEntity.setEmail(dto.getEmail());
		if (dto.getAlamat() != null) memberEntity.setAlamat(dto.getAlamat());
		if (dto.getPekerjaan() != null) memberEntity.setPekerjaan(dto.getPekerjaan());
		if (dto.getNoIdentitas() != null) memberEntity.setNoIdentitas(dto.getNoIdentitas());
		
		return memberEntity;
	}

	@Override
	public MemberDto fromEntity(MemberEntity entity) {
		if (entity == null) return null;
		return MemberDto.builder()
				.idUser(entity.getIdUser())
				.foto(entity.getFoto())
				.username(entity.getUsername())
				.password(entity.getPassword())
				.namaLengkap(entity.getNamaLengkap())
				.email(entity.getEmail())
				.alamat(entity.getAlamat())
				.pekerjaan(entity.getPekerjaan())
				.noIdentitas(entity.getNoIdentitas())
				.build();
		

	}
	

}
