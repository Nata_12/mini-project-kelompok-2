package id.kelompok2.miniproject.assembler;

import id.kelompok2.miniproject.model.dto.InputBukuDto;
import id.kelompok2.miniproject.model.entity.*;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;

@Component
public class BookAssembler implements InterfaceAssembler<BukuEntity, InputBukuDto> {

    @Override
    public BukuEntity fromDto(InputBukuDto dto) {
        if (dto == null)
            return null;

        BukuEntity entity = new BukuEntity();
        AuthorEntity authorEntity = new AuthorEntity();
        PublisherEntity publisherEntity = new PublisherEntity();
        JenisEntity jenisEntity = new JenisEntity();
        LemariEntity lemariEntity = new LemariEntity();
        if (dto.getIdBuku() != null) entity.setIdBuku(dto.getIdBuku());
        if (dto.getJudulBuku() != null) entity.setJudulBuku(dto.getJudulBuku());
        if (dto.getCover() != null) entity.setCover(dto.getCover());
        if (dto.getNamaPengarang() != null) authorEntity.setNamaPengarang(dto.getNamaPengarang());
        if (dto.getNamaPengarang() != null) publisherEntity.setNamaPenerbit(dto.getNamaPenerbit());
        if (dto.getStok() != null) entity.setStok(dto.getStok());
        if (dto.getTahunTerbit() != null) entity.setTahunTerbit(dto.getTahunTerbit());
        if (dto.getNamaJenis() != null) jenisEntity.setNamaJenis(dto.getNamaJenis());
        if (dto.getKodeLemari() != null) lemariEntity.setKodeLemari(dto.getKodeLemari());
        if (dto.getHargaBuku() != null) entity.setHargaBuku(dto.getHargaBuku());
        LocalDate date = LocalDate.now();
        entity.setTanggalMasuk(Date.valueOf(date));
        entity.setAuthorEntity(authorEntity);
        entity.setPublisherEntity(publisherEntity);
        entity.setJenisEntity(jenisEntity);
        entity.setLemariEntity(lemariEntity);
        return entity;
    }

    @Override
    public InputBukuDto fromEntity(BukuEntity entity) {
        if (entity == null)
        return null;

        return InputBukuDto.builder()
                .idBuku(entity.getIdBuku())
                .judulBuku(entity.getJudulBuku())
                .cover(entity.getCover())
                .namaPengarang(entity.getAuthorEntity().getNamaPengarang())
                .namaPenerbit(entity.getPublisherEntity().getNamaPenerbit())
                .tahunTerbit(entity.getTahunTerbit())
                .stok(entity.getStok())
                .namaJenis(entity.getJenisEntity().getNamaJenis())
                .kodeLemari(entity.getLemariEntity().getKodeLemari())
                .hargaBuku(entity.getHargaBuku())
                .tanggalMasuk(entity.getTanggalMasuk())
                .build();
    }
}
