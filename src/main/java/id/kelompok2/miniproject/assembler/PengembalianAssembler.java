package id.kelompok2.miniproject.assembler;

import java.time.LocalDate;
import java.time.Period;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.kelompok2.miniproject.model.dto.PengembalianDto;
import id.kelompok2.miniproject.model.entity.PeminjamanEntity;
import id.kelompok2.miniproject.model.entity.PengembalianEntity;
import id.kelompok2.miniproject.repository.PeminjamanRepository;

@Component
public class PengembalianAssembler implements InterfaceAssembler<PengembalianEntity, PengembalianDto> {

	@Autowired
	PeminjamanRepository pinjamRepository;
	
	@Override
	public PengembalianEntity fromDto(PengembalianDto dto) {
		if (dto == null) return null;
		PeminjamanEntity pinjam = pinjamRepository.findAllByUsernameLike(dto.getKodePeminjaman());
		PengembalianEntity entity = new PengembalianEntity();
		if(dto.getTanggalDikembalikan() != null) entity.setTanggalDikembalikan(dto.getTanggalDikembalikan());
		if(dto.getKodePeminjaman() != null) entity.setKodePeminjaman(dto.getKodePeminjaman());
		LocalDate tanggalKembali = (pinjam.getTanggalPengembalian()).toLocalDate();
		LocalDate tanggalDikembalikan = (dto.getTanggalDikembalikan()).toLocalDate();
		Integer rentang = (Period.between(tanggalKembali, tanggalDikembalikan)).getDays();
		if(rentang == 0 || rentang < 0) entity.setKeteranganKembali("Tepat Waktu");
		if(rentang > 0 ) entity.setKeteranganKembali("Terlambat "+ rentang + " hari");
		return entity;
	}

	@Override
	public PengembalianDto fromEntity(PengembalianEntity entity) {
		if(entity == null) return null;
		return PengembalianDto.builder()
				.idPengembalian(entity.getIdPengembalian())
				.username(entity.getIdPeminjaman().getUsername())
				.judulBuku(entity.getIdPeminjaman().getJudulBuku())
				.tanggalPeminjaman(entity.getIdPeminjaman().getTanggalPeminjaman())
				.tanggalPengembalian(entity.getIdPeminjaman().getTanggalPengembalian())
				.tanggalDikembalikan(entity.getTanggalDikembalikan())
				.keteranganKembali(entity.getKeteranganKembali())
				.kodePeminjaman(entity.getIdPeminjaman().getKodePeminjaman())
				.build();
				
	}
	

}
